SlugBehavior
============
SlugBehavior

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist rootmedia/yii2-slug-behavior "*"
```

or add

```
"rootmedia/yii2-slug-behavior": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \rootmedia\slug\AutoloadExample::widget(); ?>```